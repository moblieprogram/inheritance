

void main(List<String> arguments) {
  var frog = Frog();
  print(frog.name);
  frog.walk();

print("-------------------------");

  var crab = Crab();
  crab.walk();
  crab.eat();

  print("-------------------------");

  var bird = Bird();
  bird.fly();
  bird.walk();
  bird.eat();
}

mixin AquaticAnimal{
  late String name;
   void swim(){
    print(this.name+" can swim");
  }
}

mixin poultry {
  late String name;
  void fly(){
    print(this.name +" can fly");
  }
} 



class Animals{
  late String name;
void walk(){
    print(this.name+" can walk");
  }

  void eat(){
    print(this.name+" can eat");
  }

  void sleep(){
    print(this.name+" can sleep");
  }
}

class Frog extends Animals with AquaticAnimal{
@override
   String name = "Frog";

}

  class Crab extends Animals with AquaticAnimal{
    @override
   String name = "Crab";
  }

class Bird extends  Animals with poultry{
  @override
  String name="bird";
}
